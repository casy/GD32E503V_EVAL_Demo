#ifndef __VERSION_H
#define __VERSION_H

#include <string.h>
#include <stdlib.h>

typedef struct version_date 
{
   int tm_mday;        /* 月中的第几天，范围从 1 到 31*/
   int tm_mon;         /* 月份，范围从 1 到 12*/
   int tm_year;        /* 实际的年数 */       
}version_date;

extern version_date v_date;

void mcu_get_version_date(void);

#endif
