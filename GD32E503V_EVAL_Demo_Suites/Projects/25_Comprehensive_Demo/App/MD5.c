/*

	该模块仅被系统升级程序调用来进行升级代码数据的完整性验证，所以不需要加锁保护，并且把该数字签名处理模块存放在基本代码段中，
	不允许被升级掉，这样就固定了系统升级文件的签名方式，表一旦出厂即不允许被修改，如果单方面修改签名方式都会直接导致升级失败；
	算法简单描述：以64字节作为单位来分组处理输入信息；每个单位又分成16个小组，小组内有4字节；目前该算法已经被破解掉了，
	但是在本应用中是利用该数字签名来保证升级信息的完整性，实际上该数字签名可以 理解成总文件校验；所以是可用的；同时还有多种信息安全算法，比较可靠的保证升级信息的安全；
	
*/

 //https://www.cnblogs.com/chars/p/4983291.html
 //https://blog.csdn.net/goodnameused/article/details/81068697

#include  "MD5.h"



//计算公式宏定义
#define 	F(x, y, z) 		(((x) & (y)) | ((~x) & (z)))
#define 	G(x, y, z) 		(((x) & (z)) | ((y) & (~z)))
#define 	H(x, y, z) 		((x) ^ (y) ^ (z))
#define 	I(x, y, z) 		((y) ^ ((x) | (~z)))

#define 	RL(x, y) 		(((x) << (y)) | ((x) >> (32 - (y))))  //x向左循环移y位

#define 	PP(x) 			(x<<24)|((x<<8)&0xff0000)|((x>>8)&0xff00)|(x>>24)  //将x高低位互换,例如PP(aabbccdd)=ddccbbaa

#define 	FF(a, b, c, d, x, s, ac) 		a = b + (RL((a + F(b,c,d) + x + ac),s))
#define 	GG(a, b, c, d, x, s, ac) 		a = b + (RL((a + G(b,c,d) + x + ac),s))
#define 	HH(a, b, c, d, x, s, ac) 		a = b + (RL((a + H(b,c,d) + x + ac),s))
#define 	II(a, b, c, d, x, s, ac) 		a = b + (RL((a + I(b,c,d) + x + ac),s))


/*
用于bits填充的缓冲区，为什么要64个字节呢？因为当欲加密的信息的bits数被512除其余数为448时，
需要填充的bits的最大值为512=64*8 。
*/
unsigned char PADDING[]=	 	
{ 
	0x80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
 };  

// 21232f297a57a5a743894a0e4a801fc3
void MD5Init(MD5_CTX *context)        
{  
	
	 /*将当前的有效信息的长度设成0,这个很简单,还没有有效信息,长度当然是0了*/
    context->count[0] = 0;    
    context->count[1] = 0;  
	 /*初始化链接变量，算法要求这样，这个没法解释了*/
    context->state[0] = 0x67452301;  
    context->state[1] = 0xEFCDAB89;  
    context->state[2] = 0x98BADCFE;  
    context->state[3] = 0x10325476;  
	
} 
/* 

   MD5 block update operation. Continues an MD5 message-digest
   operation, processing another message block, and updating the
   context. 
   将与加密的信息传递给md5结构，可以多次调用
	 context：初始化过了的md5结构
	 input：欲加密的信息，可以任意长
	 inputLen：指定input的长度

*/

 void MD5Update(MD5_CTX *context,unsigned char *input,unsigned int inputlen)  
{  
    unsigned int i = 0,index = 0,partlen = 0; 
		/* Compute number of bytes mod 64 */
		/*计算已有信息的bits长度的字节数的模64, 64bytes=512bits。
		用于判断已有信息加上当前传过来的信息的总长度能不能达到512bits，
		如果能够达到则对凑够的512bits进行一次处理*/	
    index = (context->count[0] >> 3) & 0x3F;   // >>3   0011 1111
	
	/* Update number of bits *//*更新已有信息的bits长度*/
    partlen = 64 - index;  
    context->count[0] += inputlen << 3;  
    if(context->count[0] < (inputlen << 3))  
        context->count[1]++;  
    context->count[1] += inputlen >> 29;  //  
		/*计算已有的字节数长度还差多少字节可以 凑成64的整倍数*/
    /* Transform as many times as possible*/
		/*如果当前输入的字节数 大于 已有字节数长度补足64字节整倍数所差的字节数*/
    if(inputlen >= partlen)  
    {  
		/*用当前输入的内容把context->buffer的内容补足512bits*/
        memcpy(&context->buffer[index],input,partlen);  
			
		/*用基本函数对填充满的512bits（已经保存到context->buffer中） 做一次转换，转换结果保存到context->state中*/
			
        MD5Transform(context->state,context->buffer);  
		
		/*
		对当前输入的剩余字节做转换（如果剩余的字节<在输入的input缓冲区中>大于512bits的话 ），
		转换结果保存到context->state中
		*/
        for(i = partlen;i+64 <= inputlen;i+=64)  
            MD5Transform(context->state,&input[i]);  
        index = 0;          
    }    
    else  
    {  
        i = 0;  
    }  
		
	/* Buffer remaining input */
    /*将输入缓冲区中的不足填充满512bits的剩余内容填充到context->buffer中，留待以后再作处理*/
    memcpy(&context->buffer[index],&input[i],inputlen-i);  
}  

/* MD5 finalization. Ends an MD5 message-digest operation, writing the
   the message digest and zeroizing the context. */
/*获取加密 的最终结果
digest：保存最终的加密串
context：你前面初始化并填入了信息的md5结构
*/
void MD5Final(MD5_CTX *context,unsigned char digest[16])  
{  
    unsigned int index = 0,padlen = 0;  
    unsigned char bits[8];  
    index = (context->count[0] >> 3) & 0x3F;  
    padlen = (index < 56)?(56-index):(120-index);  
    MD5Encode(bits,context->count,8);  
    MD5Update(context,PADDING,padlen);  
    MD5Update(context,bits,8);  
    MD5Encode(digest,context->state,16);  
}  
void MD5Encode(unsigned char *output,unsigned int *input,unsigned int len)  
{  
    unsigned int i = 0,j = 0;  
    while(j < len)  
    {  
        output[j] = input[i] & 0xFF;    
        output[j+1] = (input[i] >> 8) & 0xFF;  
        output[j+2] = (input[i] >> 16) & 0xFF;  
        output[j+3] = (input[i] >> 24) & 0xFF;  
        i++;  
        j+=4;  
    }  
}  
void MD5Decode(unsigned int *output,unsigned char *input,unsigned int len)  
{  
    unsigned int i = 0,j = 0;  
    while(j < len)  
    {  
        output[i] = (input[j]) |  
            (input[j+1] << 8) |  
            (input[j+2] << 16) |  
            (input[j+3] << 24);  
        i++;  
        j+=4;   
    }  
}  


/* MD5 basic transformation. Transforms state based on block. */
/*
对512bits信息(即block缓冲区)进行一次处理，每次处理包括四轮
state[4]：md5结构中的state[4]，用于保存对512bits信息加密的中间结果或者最终结果
block[64]：欲加密的512bits信息
*/


void MD5Transform(unsigned int state[4],unsigned char block[64])  
{  
    unsigned int a = state[0];  
    unsigned int b = state[1];  
    unsigned int c = state[2];  
    unsigned int d = state[3];  
    unsigned int x[64];  
    MD5Decode(x,block,64);  
    FF(a, b, c, d, x[ 0], 7, 0xd76aa478);   
    FF(d, a, b, c, x[ 1], 12, 0xe8c7b756);   
    FF(c, d, a, b, x[ 2], 17, 0x242070db);   
    FF(b, c, d, a, x[ 3], 22, 0xc1bdceee);   
    FF(a, b, c, d, x[ 4], 7, 0xf57c0faf);   
    FF(d, a, b, c, x[ 5], 12, 0x4787c62a);   
    FF(c, d, a, b, x[ 6], 17, 0xa8304613);   
    FF(b, c, d, a, x[ 7], 22, 0xfd469501);   
    FF(a, b, c, d, x[ 8], 7, 0x698098d8);   
    FF(d, a, b, c, x[ 9], 12, 0x8b44f7af);   
    FF(c, d, a, b, x[10], 17, 0xffff5bb1);   
    FF(b, c, d, a, x[11], 22, 0x895cd7be);   
    FF(a, b, c, d, x[12], 7, 0x6b901122);   
    FF(d, a, b, c, x[13], 12, 0xfd987193);   
    FF(c, d, a, b, x[14], 17, 0xa679438e);   
    FF(b, c, d, a, x[15], 22, 0x49b40821);   
  
      
    GG(a, b, c, d, x[ 1], 5, 0xf61e2562);   
    GG(d, a, b, c, x[ 6], 9, 0xc040b340);   
    GG(c, d, a, b, x[11], 14, 0x265e5a51);   
    GG(b, c, d, a, x[ 0], 20, 0xe9b6c7aa);   
    GG(a, b, c, d, x[ 5], 5, 0xd62f105d);   
    GG(d, a, b, c, x[10], 9,  0x2441453);   
    GG(c, d, a, b, x[15], 14, 0xd8a1e681);   
    GG(b, c, d, a, x[ 4], 20, 0xe7d3fbc8);   
    GG(a, b, c, d, x[ 9], 5, 0x21e1cde6);   
    GG(d, a, b, c, x[14], 9, 0xc33707d6);   
    GG(c, d, a, b, x[ 3], 14, 0xf4d50d87);   
    GG(b, c, d, a, x[ 8], 20, 0x455a14ed);   
    GG(a, b, c, d, x[13], 5, 0xa9e3e905);   
    GG(d, a, b, c, x[ 2], 9, 0xfcefa3f8);   
    GG(c, d, a, b, x[ 7], 14, 0x676f02d9);   
    GG(b, c, d, a, x[12], 20, 0x8d2a4c8a);   
  
      
    HH(a, b, c, d, x[ 5], 4, 0xfffa3942);   
    HH(d, a, b, c, x[ 8], 11, 0x8771f681);   
    HH(c, d, a, b, x[11], 16, 0x6d9d6122);   
    HH(b, c, d, a, x[14], 23, 0xfde5380c);   
    HH(a, b, c, d, x[ 1], 4, 0xa4beea44);   
    HH(d, a, b, c, x[ 4], 11, 0x4bdecfa9);   
    HH(c, d, a, b, x[ 7], 16, 0xf6bb4b60);   
    HH(b, c, d, a, x[10], 23, 0xbebfbc70);   
    HH(a, b, c, d, x[13], 4, 0x289b7ec6);   
    HH(d, a, b, c, x[ 0], 11, 0xeaa127fa);   
    HH(c, d, a, b, x[ 3], 16, 0xd4ef3085);   
    HH(b, c, d, a, x[ 6], 23,  0x4881d05);   
    HH(a, b, c, d, x[ 9], 4, 0xd9d4d039);   
    HH(d, a, b, c, x[12], 11, 0xe6db99e5);   
    HH(c, d, a, b, x[15], 16, 0x1fa27cf8);   
    HH(b, c, d, a, x[ 2], 23, 0xc4ac5665);   
  
      
    II(a, b, c, d, x[ 0], 6, 0xf4292244);   
    II(d, a, b, c, x[ 7], 10, 0x432aff97);   
    II(c, d, a, b, x[14], 15, 0xab9423a7);   
    II(b, c, d, a, x[ 5], 21, 0xfc93a039);   
    II(a, b, c, d, x[12], 6, 0x655b59c3);   
    II(d, a, b, c, x[ 3], 10, 0x8f0ccc92);   
    II(c, d, a, b, x[10], 15, 0xffeff47d);   
    II(b, c, d, a, x[ 1], 21, 0x85845dd1);   
    II(a, b, c, d, x[ 8], 6, 0x6fa87e4f);   
    II(d, a, b, c, x[15], 10, 0xfe2ce6e0);   
    II(c, d, a, b, x[ 6], 15, 0xa3014314);   
    II(b, c, d, a, x[13], 21, 0x4e0811a1);   
    II(a, b, c, d, x[ 4], 6, 0xf7537e82);   
    II(d, a, b, c, x[11], 10, 0xbd3af235);   
    II(c, d, a, b, x[ 2], 15, 0x2ad7d2bb);   
    II(b, c, d, a, x[ 9], 21, 0xeb86d391);   
    state[0] += a;  
    state[1] += b;  
    state[2] += c;  
    state[3] += d;  
}  



/****************************************************

*****************************************************/
void MD5_TestDemo(void)
{
	unsigned char encrypt[] = "abcdegfhijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz123";//存放于加密的信息     
	unsigned char decrypt[16];        //存放加密后的结果
	
	MD5_CTX md5;
	MD5Init(&md5);       //初始化用于md5加密的结构 
	 
	MD5Update(&md5,encrypt,strlen((char *)encrypt));    //对欲加密的字符进行加密

	MD5Final(&md5,decrypt);       //获得最终结果
		                   	 
	printf("Before:%s\n",encrypt);
	printf("After:\n"); 
	char temp[2];
	for(int i = 0; i < 16; i++) // hex to str
	{
		sprintf( temp,"%02x",decrypt[i]);
		printf("%s",temp);
	}
	printf("\r\nencrypt is OK!\n");
	// 利用其它工具计算出MD5值,与以上计算的值对比其正确性
	
}



