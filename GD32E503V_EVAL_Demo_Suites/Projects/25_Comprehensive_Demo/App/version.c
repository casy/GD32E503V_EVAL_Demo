#include "version.h"

/**********************************************************

本文件包含2个功能：
1. 软件版本日期
2. mbus ID,从芯片的唯一ID号读取
软件版本号不可控,统一使用软件版本日期。
1. 读软件版本日期 格式如下：3字节协议：
Byte0 : 00~99 表示[2020 ~ 2099]
Byte1 : 1~12   月Mon
Byte2 : 1~31   日day
exp : Nov  4 2020
*********************************************************/

char  TimeStr[]= __TIME__;
char  DataStr[]= __DATE__; //"Nov  4 2020";
 
version_date v_date;

char Month_tab[][12] = 
{
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",	
	"August",
	"September",
	"September",
	"November",
	"December",
};

/*
获取编译后的字符日期版本,转换成整数后保存
// Nov  4 2020
*/
void mcu_get_version_date(void)
{
	/*比较前3个字符,找出月份*/
	for(int i = 0; i < 12; i++)
	{
		char *ptr = Month_tab[i];	
		if(*ptr == DataStr[0])
		{
			ptr++;	
			if(*ptr == DataStr[1])
			{
				ptr++;	
				if(*ptr == DataStr[2])
				{
					v_date.tm_mon = i+1;
					break;			
				}			
			}
		}
	}
	/*找出日+年*/	
   char *pstr = DataStr;
   pstr = strchr(pstr, ' '); /*寻找空格*/
   pstr++;	
   if(*pstr == ' ')  /*如果为1~9日,则字符串中隔有2个空格*/
   {	
	  pstr++;	
	  // 字符转数字  
	 char b = *pstr;   
	 v_date.tm_mday = (b - 0x30);		
   }
   else
   {
	 char a = *pstr;  
	 pstr++;	
	 char b = *pstr;   
	 v_date.tm_mday = (a - 0x30)*10 + (b - 0x30);	 
   }
   pstr++; 
   v_date.tm_year = atoi(pstr); 
}










