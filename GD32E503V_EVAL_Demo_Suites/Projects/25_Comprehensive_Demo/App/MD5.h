 
#ifndef  __MD5_H__
#define  __MD5_H__
 
#include  <string.h>
#include <stdio.h> 
 
typedef struct  
{	
 unsigned int count[2]; 	/*存储原始信息的bits数长度,不包括填充的bits，最长为 2^64 bits，因为2^64是一个64位数的最大值*/
 unsigned int state[4]; 	/*四个32bits数，用于存放最终计算得到的消息摘要。当消息长度〉512bits时，也用于存放每个512bits的中间结果*/
 unsigned char buffer[64];		
}MD5_CTX; 
 
 
void MD5Init(MD5_CTX *context);  

void MD5Update(MD5_CTX *context,unsigned char *input,unsigned int inputlen);  
void MD5Final(MD5_CTX *context,unsigned char digest[16]);	
void MD5Transform(unsigned int state[4],unsigned char block[64]);	
void MD5Encode(unsigned char *output,unsigned int *input,unsigned int len);  
void MD5Decode(unsigned int *output,unsigned char *input,unsigned int len);  

void MD5_TestDemo(void);
 
#endif
 
