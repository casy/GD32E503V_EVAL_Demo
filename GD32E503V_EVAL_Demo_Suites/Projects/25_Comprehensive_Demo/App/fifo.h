#ifndef __FIFO_H__
#define __FIFO_H__


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define BUFF_Size  200 // 512u
extern uint8_t fifo_com1_rx[BUFF_Size];

typedef enum 
{
   ERR_FIFO_FULL,  
   ERR_FIFO_EMPTY,  
   ERR_FIFO_SPACE_NOT_ENOUGH,
   ERR_FIFO_LEN_NOT_ENOUGH,
   ERR_ADDR,
   OK,
}enum_fifo;


/*!
 * FIFO structure
 */
typedef struct Fifo_s
{
    uint16_t Begin;
    uint16_t End;
    uint8_t *Data;
    uint16_t Size;
}Fifo_t;

extern Fifo_t fifo_com1;

/*!
 * Initializes the FIFO structure
 *
 * \param [IN] fifo   Pointer to the FIFO object
 * \param [IN] buffer Buffer to be used as FIFO
 * \param [IN] size   Size of the buffer
 */
void FifoInit( Fifo_t *fifo, uint8_t *buffer, uint16_t size );

/*!
 * Pushes data to the FIFO
 *
 * \param [IN] fifo Pointer to the FIFO object
 * \param [IN] data Data to be pushed into the FIFO
 */
void FifoPush( Fifo_t *fifo, uint8_t data );

/*!
 * Pops data from the FIFO
 *
 * \param [IN] fifo Pointer to the FIFO object
 * \retval data     Data popped from the FIFO
 */
uint8_t FifoPop( Fifo_t *fifo );

/*!
 * Flushes the FIFO
 *
 * \param [IN] fifo   Pointer to the FIFO object
 */
void FifoFlush( Fifo_t *fifo );

/*!
 * Checks if the FIFO is empty
 *
 * \param [IN] fifo   Pointer to the FIFO object
 * \retval isEmpty    true: FIFO is empty, false FIFO is not empty
 */
bool IsFifoEmpty( Fifo_t *fifo );

/*!
 * Checks if the FIFO is full
 *
 * \param [IN] fifo   Pointer to the FIFO object
 * \retval isFull     true: FIFO is full, false FIFO is not full
 */
bool IsFifoFull( Fifo_t *fifo );

uint16_t FifoDepth( Fifo_t *fifo );

void FifoTest(Fifo_t *fifo, uint16_t size);


/**
 *****************************************************************************
 ** \brief 向队列中插入N个数据
 **
 **
 ** \param 1 : fifo 队列块,  2*data 数据指针  3：数据长度
 ** 
 ** \retval  状态码                                  
 *****************************************************************************/
enum_fifo en_Queue( Fifo_t *fifo, uint8_t *data, uint16_t lentghBytes);


/**
 *****************************************************************************
 ** \brief 从队列中取出 N个数据
 **
 **
 ** \param 1 : fifo 队列块,  2*data 数据指针  3：数据长度
 ** 
 ** \retval  状态码                                  
 *****************************************************************************/
enum_fifo de_Queue(Fifo_t *fifo, uint8_t *data, uint16_t lentghBytes);










	



#endif // __FIFO_H__



