/*!
    \file    picture.h
    \brief   picture header file

    \version 2020-09-04, V1.0.0, demo for GD32E50x
*/

#ifndef PICTURE_H
#define PICTURE_H

#include "gd32e50x.h"

/*
取模有一个说明文档.docx
*/
#define  BMP_HEADSIZE    (8)  /*8 图片取模后有个结构体占用了8个字节*/


typedef struct
{
	unsigned char scan;
	unsigned char gray;
	unsigned short w;
	unsigned short h;
	unsigned char is565;
	unsigned char rgb;
}IMG_Head;  /*图片头*/


extern const uint8_t picture[];
extern const uint8_t image_off[];
extern const uint8_t image_on[];
extern const uint8_t gImage_maomao[153608]; 

#endif /* PICTURE_H */
