
#ifndef _LITTLEFS_PORT_H_
#define _LITTLEFS_PORT_H_

#include "lfs.h"
#include "bsp_gd25qxx.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void File_System_Init(void);
int8_t File_Write(char *name, uint8_t *dat, uint16_t bypass, uint16_t size);
lfs_size_t File_Read(char *name, uint8_t *dat, lfs_soff_t off, lfs_size_t size);
void littlefs_test(void);


#endif
