#include "test.h"



void LCD_Disp_ButtonLogo(void)
{
	char_format_struct char_format;
	uint16_t a1=20;
	uint16_t a2=120;
	uint16_t a3=220;
	uint16_t b1=10;
	uint16_t b2=100;
	uint16_t b3=200;
	uint8_t led_string[4][4]={"LED1","LED2","LED3","LED4"};
	uint16_t i;
		
    /* draw the picture of Gigadevice logo */
    /* if you don't want to draw the picture, you should modify the macro on
       the line 445th of picture.c file and comment the next line */
    lcd_picture_draw(60,210,60 + 120 -1,210 + 95 - 1,(uint16_t *)(picture + BMP_HEADSIZE));

    /* draw a rectangle */
    lcd_rectangle_draw(10,10,230,310,LCD_COLOR_BLUE);

    /* configure char format */
    char_format.char_color = LCD_COLOR_BLUE;
    char_format.bk_color = LCD_COLOR_WHITE;
    char_format.direction = CHAR_DIRECTION_VERTICAL;
    char_format.font = CHAR_FONT_8_16;  
    /* draw character on LCD screen */
    for (i = 0; i < 4; i++)
	{
        lcd_char_display((a1+35+8*i), b1+20, *(led_string[0]+i), char_format);
        lcd_char_display((a2+35+8*i), b1+20, *(led_string[1]+i), char_format);
        lcd_char_display((a1+35+8*i), b2+20, *(led_string[2]+i), char_format);
        lcd_char_display((a2+35+8*i), b2+20, *(led_string[3]+i), char_format);
    }
    	
    /* draw picture of button on LCD screen */
    lcd_picture_draw(a1+30,b1+40,a1+30+40-1,b1+40+40-1,(uint16_t *)(image_off + BMP_HEADSIZE));
    lcd_picture_draw(a2+30,b1+40,a2+30+40-1,b1+40+40-1,(uint16_t *)(image_off + BMP_HEADSIZE));
    lcd_picture_draw(a1+30,b2+40,a1+30+40-1,b2+40+40-1,(uint16_t *)(image_off + BMP_HEADSIZE));
    lcd_picture_draw(a2+30,b2+40,a2+30+40-1,b2+40+40-1,(uint16_t *)(image_off + BMP_HEADSIZE));

}

