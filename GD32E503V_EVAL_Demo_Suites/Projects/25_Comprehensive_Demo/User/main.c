/*!
    \file    main.c
    \brief   running led

    \version 2020-09-04, V1.0.0, demo for GD32E50x
*/

/*
    Copyright (c) 2020, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/


#include "systick.h"

#include "gd32e503v_eval.h"
#include "gd32e503v_lcd_eval.h"

#include "bsp_touch.h"
#include "bsp_gd25qxx.h"
#include "bsp_sdcard.h"

#include "picture.h"

#include "FreeRTOS.h"
#include "task.h"

#include "littlefs_port.h"


#define LED1_TASK_PRIO    ( tskIDLE_PRIORITY + 2 )

void LED1_task(void * pvParameters);

void LCD_Disp_ButtonLogo(void);


/*!
    \brief      get chip serial number
    \param[in]  none
    \param[out] none
    \retval     none
*/
void get_chip_serial_num(void)
{
	uint32_t int_device_serial[3];  
	int_device_serial[0] = *(__IO uint32_t*)(0x1FFFF7E0);
    int_device_serial[1] = *(__IO uint32_t*)(0x1FFFF7EC);
    int_device_serial[2] = *(__IO uint32_t*)(0x1FFFF7F0);
	/* printf CPU unique device id */
    printf("\n\rGD32E503V-EVAL-V1.0 The CPU Unique Device ID:[%X-%X-%X]\n\r",int_device_serial[2],int_device_serial[1],int_device_serial[0]);                       

}

static void led_init(void)
{
    /* initialize the leds */
    gd_eval_led_init(LED1);
    gd_eval_led_init(LED2);
    gd_eval_led_init(LED3);
    gd_eval_led_init(LED4);

    /* close all of leds */
    gd_eval_led_off(LED1);
    gd_eval_led_off(LED2);
    gd_eval_led_off(LED3);
    gd_eval_led_off(LED4);
}



int main(void)
{
    //systick_config();
	
	led_init();
	
	 /* configure EVAL_COM0 */
    gd_eval_com_init(EVAL_COM0);

    /* configure TAMPER key */
    gd_eval_key_init(KEY_B, KEY_MODE_GPIO);
	
	BSP_Lcd_init(LCD_COLOR_WHITE);
  
	BSP_SDCard_init();
	BSP_SDCard_info_get();
	
    /* configure the GPIO of SPI touch panel */
    touch_panel_gpio_configure();

    //delay_1ms(50);  

    /* GD32E503Z-EVAL-V1.0 Start up */
   
    printf("\n\rGD32E503V-EVAL-V1.0 System is Starting up...\n\r");
    printf("\n\rGD32E503V-EVAL-V1.0 Flash Size :%dK\n\r",*(__IO uint16_t*)(0x1FFFF7E0));
  
    /* get chip serial number */
    get_chip_serial_num();
    spi_flash_init();
    printf("\n\rGD32E503V-EVAL-V1.0 SPI Flash:GD25Q16 configured...\n\r");
    /* get flash id */
    int flash_id = spi_flash_read_id();
    printf("\n\rThe Flash_ID:0x%X\n\r",flash_id);
	
	SPI_Flash_Test();

	//File_System_Init();
	//littlefs_test();
	
	//LCD_Disp_ButtonLogo();
	
	char_format_struct char_format;
	
	char error_string[]="Hello,eeworld!";
	
	/* configure char format */
    char_format.char_color = LCD_COLOR_RED;
    char_format.bk_color = LCD_COLOR_WHITE;
    char_format.font = CHAR_FONT_8_16; 
	
	lcd_str_display(8,8, error_string, strlen(error_string), char_format);
	lcd_picture_draw(0,0,(uint16_t *)(gImage_maomao));
	
	//lcd_chinese_display(0,50,"电子工程世界",char_format);
	
	//MD5_TestDemo();

    nvic_priority_group_set(NVIC_PRIGROUP_PRE4_SUB0);
	
	xTaskCreate(LED1_task, "LED1", configMINIMAL_STACK_SIZE, NULL, LED1_TASK_PRIO, NULL);
	
	/* start scheduler */
    vTaskStartScheduler();
	
    while(1){
    }
	
}


void LED1_task(void * pvParameters)
{  
    for( ;; ){
        /* toggle LED2 each 500ms */
        gd_eval_led_toggle(LED1);
        vTaskDelay(100);
    }
}



void LCD_Disp_ButtonLogo(void)
{
	char_format_struct char_format;
	uint16_t a1=20;
	uint16_t a2=120;
	uint16_t a3=220;
	uint16_t b1=10;
	uint16_t b2=100;
	uint16_t b3=200;
	uint8_t led_string[4][4]={"LED1","LED2","LED3","LED4"};
	uint16_t i;
		
    /* draw the picture of Gigadevice logo */
    /* if you don't want to draw the picture, you should modify the macro on
       the line 445th of picture.c file and comment the next line */
    //lcd_picture_draw(60,210,60 + 120 -1,210 + 95 - 1,(uint16_t *)(picture + BMP_HEADSIZE));

	//gImage_maomao

    /* draw a rectangle */
    lcd_rectangle_draw(10,10,230,310,LCD_COLOR_BLUE);

    /* configure char format */
    char_format.char_color = LCD_COLOR_BLUE;
    char_format.bk_color = LCD_COLOR_WHITE;

    char_format.font = CHAR_FONT_8_16;  
    /* draw character on LCD screen */
//    for (i = 0; i < 4; i++){
//        lcd_char_display((a1+35+8*i), b1+20, *(led_string[0]+i), char_format);
//        lcd_char_display((a2+35+8*i), b1+20, *(led_string[1]+i), char_format);
//        lcd_char_display((a1+35+8*i), b2+20, *(led_string[2]+i), char_format);
//        lcd_char_display((a2+35+8*i), b2+20, *(led_string[3]+i), char_format);
//    }
    	
    /* draw picture of button on LCD screen */
//    lcd_picture_draw(a1+30,b1+40,a1+30+40-1,b1+40+40-1,(uint16_t *)(gImage_ee + BMP_HEADSIZE));
//    lcd_picture_draw(a2+30,b1+40,a2+30+40-1,b1+40+40-1,(uint16_t *)(gImage_ee + BMP_HEADSIZE));
//    lcd_picture_draw(a1+30,b2+40,a1+30+40-1,b2+40+40-1,(uint16_t *)(image_off + BMP_HEADSIZE));
//    lcd_picture_draw(a2+30,b2+40,a2+30+40-1,b2+40+40-1,(uint16_t *)(image_off + BMP_HEADSIZE));

}
